package com.adayveed.sqlitedb.dbaccess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by alamzdayveed on 6/2/2016.
 */
public class ToursDataSource {
    private static final String LOGTAG = "EXPLORECA";

    SQLiteOpenHelper dbhelper;
    SQLiteDatabase database;

    public ToursDataSource(Context context) {
        dbhelper = new ToursDBOpenHelper(context);
    }

    public void open() {
        Log.i(LOGTAG, "Database opened");
        database = dbhelper.getWritableDatabase();
    }

    public void close() {
        Log.i(LOGTAG, "Database closed");
        dbhelper.close();
    }
}
